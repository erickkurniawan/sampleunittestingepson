﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContohUnitTest1.Test
{
    [TestClass]
    public class ShoppingCartTestAssembly2
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void Assembly_ClassTest_Pada_Class_Berbeda()
        {
            int expected = ShoppingCartTestAssembly.Cart.ItemCount + 1;
            ShoppingCartTestAssembly.Cart.Add(new Item
            {
                ItemName = "Nitendo Swift"
            });
            Assert.AreEqual(expected, ShoppingCartTestAssembly.Cart.ItemCount);

            TestContext.WriteLine(TestContext.TestName);
        }
    }
}
