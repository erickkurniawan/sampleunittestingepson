﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContohUnitTest1.Test
{
    [TestClass]
    public class TestPasswordTests
    {
        [TestMethod]
        public void Cek_Jika_Password_harus_min7kar_AngkaHurufBesarKecil()
        {
            string password = "PassW0rd123";
            int expected = 4;

            Assert.AreEqual(TestPassword.GetPassStrength(password), expected);
        }
    }
}
