﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContohUnitTest1.Test
{
    [TestClass]
    public class DataDrivenTest
    {
        private static UserDetailEntry user;

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            user = new UserDetailEntry();
        }

        public TestContext TestContext { get; set; }

        //menambahkan xml
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
        //    "|DataDirectory|\\UserDetails.xml", "User", 
        // DataAccessMethod.Sequential),
        // TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\\UserDetails.csv", 
            "UserDetails.csv", DataAccessMethod.Sequential),
            DeploymentItem("ContohUnitTest1.Test\\UserDetails.csv"),TestMethod]
        public void DataTest()
        {
            string userId = Convert.ToString(TestContext.DataRow["userid"]);
            string telephone = Convert.ToString(TestContext.DataRow["telephone"]);
            string email = Convert.ToString(TestContext.DataRow["email"]);

            bool result = user.Add(userId, telephone, email);
            Assert.IsTrue(result);
        }
    }
}
