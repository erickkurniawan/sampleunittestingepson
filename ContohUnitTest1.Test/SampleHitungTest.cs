﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ContohUnitTest1.Test
{
    [TestClass]
    public class SampleHitungTest
    {
        [TestMethod]
        public void Cek_Hitung_Luas_Sesuai_Rumus()
        {
            //arrange jika tinggi 10 dan alas 10
            double expected = 50;

            //act
            SampleHitung objSample = new SampleHitung();
            double hasil = objSample.HitungLuasSegitiga(10, 10);

            //assert
            Assert.AreEqual(expected, hasil);
        }

        [TestMethod]
        public void Cek_Bil_Prima_Jika_Dimasukan_Angka_1()
        {
            bool expected = false;

            SampleHitung sampleHitung = new SampleHitung();
            bool hasil = sampleHitung.CekBilanganPrima(1);

            Assert.AreEqual(expected, hasil);
        }

        [TestMethod]
        public void Cek_Bil_Prima_Jika_Dimasukan_Angka_5()
        {
            bool expected = true;

            SampleHitung sampleHitung = new SampleHitung();
            bool hasil = sampleHitung.CekBilanganPrima(5);

            Assert.AreEqual(expected, hasil);
        }
    }
}
