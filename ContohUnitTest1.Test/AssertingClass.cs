﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContohUnitTest1.Test
{
    [TestClass]
    public class AssertingClass
    {
        static List<string> employee;

        [ClassInitialize]
        public static void CreateData(TestContext context)
        {
            employee = new List<string>() { "Budi","Bambang","Amir",
            "Edo","Erick"};
        }

        //first commit
        [ClassCleanup]
        public static void CleanUp()
        {

        }

        [TestMethod]
        public void cek_koleksi_null_atau_tidak()
        {
            CollectionAssert.AllItemsAreNotNull(employee, "Not null failed");
        }

        [TestMethod]
        public void cek_koleksi_unique()
        {
            CollectionAssert.AllItemsAreUnique(employee);
        }

        [TestMethod]
        public void cek_equality_koleksi()
        {
            var hasil = new List<string>() { "Budi","Bambang","Amir",
            "Edo","Erick"};
            CollectionAssert.AreEqual(employee, hasil);
        }

        //menambahkan comment
        [TestMethod]
        public void cek_equivalen_koleksi()
        {
            var hasil = new List<string>() {"Erick","Budi","Bambang","Amir",
            "Edo"};
            CollectionAssert.AreEquivalent(employee, hasil);
        }

        [TestMethod]
        public void cek_string_contains()
        {
            var hasil = "Belajar MSTest untuk Unit Testing";

            StringAssert.Contains(hasil, "Unit");
        }

        //menambahkan test method
        [TestMethod]
        public void cek_stringassert_with_regex()
        {
            StringAssert.Matches("Hello World", 
                new System.Text.RegularExpressions.Regex(@"\s"));
            StringAssert.StartsWith("Hello World", "Hello");
            StringAssert.EndsWith("Hello World", "World");
        }

        [TestMethod]
        public void cek_subset_koleksi() {
            var hasil = new List<string>() {"Amir",
            "Edo"};
            CollectionAssert.IsSubsetOf(hasil,employee);
        }

        [TestMethod]
        public void Test_Same_String()
        {
            string a = "Hello";
            string b = "Hello";
            //Assert.AreEqual(a, b,true);
            Assert.AreSame(a, b);
        }
    }
}
