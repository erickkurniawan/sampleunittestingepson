﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContohUnitTest1.Test
{
    [TestClass]
    public class ShoppingCartTestAssembly
    {
        public TestContext TestContext { get; set; }
        private static ShoppingCart _cart;
        public static ShoppingCart Cart
        {
            get { return _cart; }
            set { _cart = value; }
        }

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            _cart = new ShoppingCart();
            _cart.Add(new Item { ItemName = "Nitendo Swift", ItemQuantity = 11 });
            context.WriteLine("Assembly Initialize");
        }

        [TestMethod]
        public void Cek3_Menambahkan_Satu_Item_Kedalam_Cart()
        {
            int expected = _cart.ItemCount + 1;
            _cart.Add(new Item { ItemName = "Samsung S9", ItemQuantity = 10 });

            Assert.AreEqual(expected, _cart.ItemCount);
        }

        [AssemblyCleanup]
        public static void AssemblyCleanUp()
        {
            _cart.Dispose();
        }

    }
}
