﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContohUnitTest1
{
    public class UserDetailEntry
    {
        public bool Add(string userId, string telephone, string email)
        {
            if (userId.Length < 4)
                throw new Exception("UserId should be greater than 4 char!");
            if (telephone.Contains("a"))
                throw new Exception("Telephone should not contain alphabets !");
            if (!email.Contains("@"))
                throw new Exception("Email should contains @");

            return true;
        }
    }
}
