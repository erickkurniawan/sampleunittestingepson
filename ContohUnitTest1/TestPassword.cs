﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContohUnitTest1
{
    public class TestPassword
    {
        public static int GetPassStrength(string password)
        {
            if (string.IsNullOrEmpty(password))
                return 0;

            int result = 0;

            if (Math.Max(password.Length, 7) > 7)
                result += 1;

            if (System.Text.RegularExpressions.Regex.Match(password,
                "[0-9]").Success)
                result += 1;

            if (System.Text.RegularExpressions.Regex.Match(password, "[a-z]").Success)
                result += 1;

            if (System.Text.RegularExpressions.Regex.Match(password, "[A-Z]").Success)
                result += 1;

            return result;
        }
    }
}
