﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContohUnitTest1
{
    public class ShoppingCart : IDisposable
    {
        public List<Item> Items = new List<Item>();

        public int ItemCount
        {
            get { return Items.Count; }
        }

        public void Add(Item item)
        {
            Items.Add(item);
        }

        public void Dispose()
        {
            
        }

        public void Remove(int index)
        {
            this.Items.RemoveAt(index);
        }
    }
}
