﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContohUnitTest1
{
    public class SampleHitung
    {
        public double HitungLuasSegitiga(double alas,double tinggi)
        {
            double hasil = 0.5 * alas * tinggi;
            return hasil;
        }

        public bool CekBilanganPrima(int bil)
        {
            int count = 0;
            for(int i = 1; i <= bil; i++)
            {
                if (bil % i == 0)
                {
                    count++;
                }
            }

            if (count == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
